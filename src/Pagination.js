import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import styles from "./pagination.module.css";

const cx = classNames.bind(styles);

const Pagination = ({ metadata, handlePage, currentPage }) => {
  if (!metadata) {
    return;
  }

  const { total_count: total, per_page: perPage } = metadata;
  const totalPages = Math.ceil(total / perPage);
  const pages = [...Array(totalPages).keys()];

  const handlePageClick = (e, pageNumber) => {
    handlePage(pageNumber);
  };

  const buttonClasses = pageNumber => {
    return cx({
      [styles.pageLink]: true,
      [styles.currentPageLink]: currentPage === pageNumber
    });
  };

  return (
    <nav className={styles.nav}>
      {pages.map(index => {
        const displayPageNumber = index + 1;
        return (
          <div className={styles.page} key={displayPageNumber}>
            <button
              onClick={e => handlePageClick(e, displayPageNumber)}
              className={buttonClasses(displayPageNumber)}
            >
              {displayPageNumber}
            </button>
          </div>
        );
      })}
    </nav>
  );
};

Pagination.propTypes = {
  metadata: PropTypes.object.isRequired,
  handlePage: PropTypes.func.isRequired,
  currentPage: PropTypes.number.isRequired
};

export default Pagination;
