import Vehicles from "./vehicles";

let instance;
const jsonMock = jest
  .fn()
  .mockReturnValue({ data: [{ id: 1 }], metadata: { some_value: 1 } });

const fetchMock = jest.fn().mockImplementation(() => {
  return { json: jsonMock };
});

describe("Vehicles service", () => {
  beforeEach(() => {
    instance = new Vehicles();
    global.fetch = fetchMock;
  });

  describe("it sends off a request to drover with the correct options", () => {
    it("calls the mock with the correct args", async () => {
      const args = { page: 1, vehicle_type: "PCO" };

      await instance.retrieve(args);

      expect(fetchMock).toHaveBeenCalledWith(
        "https://app.joindrover.com/api/web/vehicles",
        {
          method: "POST",
          body: JSON.stringify(args),
          headers: { "content-type": "application/json" }
        }
      );
    });

    it("returns the return value of the mock", async () => {
      const args = { page: 1, vehicle_model: "some value" };

      const result = await instance.retrieve(args);

      expect(result).toEqual({
        data: [{ id: 1 }],
        metadata: { some_value: 1 }
      });
    });
  });
});
