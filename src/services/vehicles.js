export default class Vehicles {
  async retrieve(options = {}) {
    const response = await fetch(
      "https://app.joindrover.com/api/web/vehicles",
      {
        method: "POST",
        body: JSON.stringify(options),
        headers: { "content-type": "application/json" }
      }
    );

    return response.json();
  }
}
