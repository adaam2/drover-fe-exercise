import React from "react";
import { shallow } from "enzyme";
import Moment from "moment";

import VehicleCard from "./VehicleCard";

let wrapper;
const today = new Moment();

describe("<VehicleCard />", () => {
  beforeEach(() => {
    wrapper = shallow(
      <VehicleCard
        id="blah"
        year="2018"
        available_start_date={today}
        city_name="St Albans"
        vehicle_make="Skoda"
        vehicle_model="Something"
        images={[{ image_url: "https://somedomain.com/image" }]}
      />
    );
  });

  it("renders the top image", () => {
    expect(wrapper.html()).toEqual(
      expect.stringContaining(
        "background-image:url(https://somedomain.com/image)"
      )
    );
  });

  it("renders the correct title", () => {
    const h4 = wrapper.find("h4");

    expect(h4).toHaveLength(1);
    expect(h4.text()).toEqual("Skoda - Something (2018)");
  });

  it("renders the link to the detail page", () => {
    const link = wrapper.find("a.cardButton");

    expect(link).toHaveLength(1);
    expect(link.hasClass("cardButton")).toEqual(true);
    expect(link.text()).toEqual("Available NOW");
  });

  describe("When the available now date is in the future", () => {
    beforeEach(() => {
      wrapper = shallow(
        <VehicleCard
          id="blah"
          year="2018"
          available_start_date={today.add(5, "days")}
          city_name="St Albans"
          vehicle_make="Skoda"
          vehicle_model="Something"
          images={[{ image_url: "https://somedomain.com/image" }]}
        />
      );
    });

    it("renders the correct 'available in' text", () => {
      const hyperlink = wrapper.find("a");
      expect(hyperlink.text()).toEqual("Available in 5 days");
    });
  });
});
