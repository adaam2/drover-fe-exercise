import React from "react";
import { shallow } from "enzyme";

import Pagination from "./Pagination";

const handlePageMock = jest.fn();

let wrapper;

describe("<Pagination />", () => {
  describe("With many pages", () => {
    beforeEach(() => {
      const metadata = {
        total_count: 250,
        per_page: 50
      };
      wrapper = shallow(
        <Pagination
          currentPage={3}
          metadata={metadata}
          handlePage={handlePageMock}
        />
      );
    });

    it("renders the correct number of pages", () => {
      expect(wrapper.find(".pageLink")).toHaveLength(5);
    });

    it("renders the correct current page item", () => {
      const currentPageItem = wrapper.find(".currentPageLink");
      expect(currentPageItem.text()).toEqual("3");
      expect(currentPageItem.hasClass("currentPageLink")).toEqual(true);
    });

    it("triggers the pagination func when clicked on an item", () => {
      const firstPage = wrapper.find(".pageLink").first();
      firstPage.simulate("click");

      expect(handlePageMock).toHaveBeenCalledWith(1);

      const anotherPage = wrapper.find(".pageLink").at(2);
      anotherPage.simulate("click");

      expect(handlePageMock).toHaveBeenCalledWith(3);
    });
  });

  describe("With no pages", () => {
    beforeEach(() => {
      const metadata = {
        total_count: 0,
        per_page: 50
      };

      wrapper = shallow(
        <Pagination
          currentPage={1}
          metadata={metadata}
          handlePage={handlePageMock}
        />
      );
    });

    it("does not render any page links", () => {
      expect(wrapper.find(".pageLink")).toHaveLength(0);
    });
  });

  describe("With no metadata prop passed", () => {
    beforeEach(() => {
      const metadata = undefined;

      wrapper = shallow(
        <Pagination
          currentPage={1}
          metadata={metadata}
          handlePage={handlePageMock}
        />
      );
    });

    it("renders nothing", () => {
      expect(wrapper.html()).toEqual(null);
    });
  });
});
