import React from "react";
import { shallow } from "enzyme";

import Facets from "./Facets";

let wrapper;
const handleFilterMock = jest.fn();
const handleVehicleTypeChange = jest.fn();

describe("<Facets />", () => {
  beforeEach(() => {
    wrapper = shallow(
      <Facets
        metadata={{
          aggregations: {
            body_information: {
              hatchback: 3,
              estate: 12
            },
            fuel: {
              diesel: 30,
              petrol: 12
            }
          }
        }}
        filters={{
          page: 1,
          per_page: 9,
          // Display filters below
          vehicle_make: "Skoda",
          vehicle_model: "Something"
        }}
        handleFilter={handleFilterMock}
        handleVehicleTypeChange={handleVehicleTypeChange}
      />
    );
  });

  it("renders the correct facets", () => {
    const facetSections = wrapper.find(".facetSection");
    const facets = facetSections.map(facetSection => {
      const title = facetSection.find("legend.facetSectionTitle").text();
      const items = facetSection.find(".facetOptionsContainer").children();

      return {
        title,
        items
      };
    });

    expect(facets.map(f => f.title)).toEqual(["Body information", "Fuel"]);
  });

  it("renders the correct applied filters", () => {
    const appliedFilters = wrapper.find(".appliedFilter");

    const appliedFilterValues = appliedFilters.map(filter => {
      return filter
        .find("i[data-identifier]")
        .first()
        .text();
    });

    expect(appliedFilterValues).toEqual(["Skoda", "Something"]);
  });

  describe("Removing an applied filter", () => {
    it("calls the handleFilterMock with the correct params", () => {
      const firstApplied = wrapper
        .find(".appliedFilter")
        .first()
        .find("button");

      firstApplied.simulate("click");

      expect(handleFilterMock).toHaveBeenCalledWith(
        "vehicle_make",
        "Skoda",
        false
      );
    });
  });

  describe("Changing the type of hire vehicle to PCO or Consumer", () => {
    it("calls the handleVehicleTypeChangeMock with the correct value", () => {
      const select = wrapper.find("select");

      select.simulate("change", { currentTarget: { value: "PCO" } });

      expect(handleVehicleTypeChange).toHaveBeenCalledWith("PCO");

      select.simulate("change", { currentTarget: { value: "Consumer" } });

      expect(handleVehicleTypeChange).toHaveBeenCalledWith("Consumer");
    });
  });
});
