import React from "react";
import { shallow } from "enzyme";

import FacetItem from "./FacetItem";

const handleFilter = jest.fn();

let wrapper;

describe("<FacetItem />", () => {
  describe("When the facet item is unselected", () => {
    beforeEach(() => {
      wrapper = shallow(
        <FacetItem
          selected={false}
          section="vehicle_make"
          value="Skoda"
          handleFilter={handleFilter}
          count={5}
        />
      );
    });

    it("renders the expected html", () => {
      expect(wrapper.html()).toEqual(
        '<div class="item"><span>Skoda (5)</span></div>'
      );
    });

    describe("Filtering on a facet item", () => {
      it("calls the handleFilter mock with the correct params", () => {
        const item = wrapper.find("span");
        item.simulate("click");

        expect(handleFilter).toHaveBeenCalledWith(
          "vehicle_make",
          "Skoda",
          true
        );
      });
    });
  });

  describe("When the item is already selected", () => {
    beforeEach(() => {
      wrapper = shallow(
        <FacetItem
          selected={true}
          section="vehicle_model"
          value="Something"
          handleFilter={handleFilter}
          count={3}
        />
      );
    });

    it("renders the expected html output", () => {
      expect(wrapper.html()).toEqual(
        '<div class="item selected"><span>Something (3)</span></div>'
      );
    });

    describe("Unfiltering the selected filter", () => {
      it("calls the handleFilter mock with the correct args", () => {
        const item = wrapper.find("span");

        item.simulate("click");

        expect(handleFilter).toHaveBeenCalledWith(
          "vehicle_model",
          "Something",
          false
        );
      });
    });
  });
});
