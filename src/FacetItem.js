import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames/bind";

import styles from "./facet-item.module.css";

const cx = classNames.bind(styles);

const FacetItem = ({ selected, section, value, handleFilter, count }) => {
  const itemClasses = () =>
    cx({
      [styles.item]: true,
      [styles.selected]: selected
    });

  const handleClick = () => {
    handleFilter(section, value, !selected);
  };

  return (
    <div className={itemClasses()} key={value}>
      <span onClick={handleClick}>
        {value} ({count})
      </span>
    </div>
  );
};

FacetItem.propTypes = {
  section: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  handleFilter: PropTypes.func.isRequired,
  count: PropTypes.number.isRequired
};

export default FacetItem;
