import React from "react";
import { shallow } from "enzyme";
import Moment from "moment";

import VehicleResults from "./VehicleResults";
import VehicleCard from "./VehicleCard";
import Pagination from "./Pagination";

let wrapper;
const handlePage = jest.fn();

describe("<VehicleResults />", () => {
  beforeEach(() => {
    wrapper = shallow(
      <VehicleResults
        results={[
          {
            id: 1,
            images: [{ image_url: "some/url" }],
            vehicle_make: "Make",
            vehicle_model: "Model",
            year: "2017",
            available_from: new Moment()
          },
          {
            id: 2,
            images: [{ image_url: "some/url2" }],
            vehicle_make: "Another Make",
            vehicle_model: "Another Model",
            year: "2018",
            available_from: new Moment().add(5, "days")
          }
        ]}
        metadata={{
          per_page: 25,
          total_count: 2
        }}
        handlePage={handlePage}
        currentPage={1}
      />
    );
  });

  it("renders the resultsContainer", () => {
    const resultsContainer = wrapper.find(".resultsContainer");
    expect(resultsContainer).toHaveLength(1);
  });

  it("renders the correct number of cards", () => {
    const cards = wrapper.find(VehicleCard);
    expect(cards).toHaveLength(2);
  });

  it("renders the pagination", () => {
    const pagination = wrapper.find(Pagination);
    expect(pagination).toHaveLength(1);
  });
});
