import React from "react";
import { shallow } from "enzyme";

import App from "./App";
import Vehicles from "./services/vehicles";

const mockRetrieve = jest.fn().mockReturnValue({
  data: [],
  metadata: undefined
});

jest.mock("./services/vehicles.js");

Vehicles.mockImplementation(() => {
  return { retrieve: mockRetrieve };
});

let wrapper;

describe("<App />", () => {
  beforeEach(() => {
    jest.clearAllMocks();
    wrapper = shallow(<App />);
  });

  describe("Initialization of default filters", () => {
    it("initializes the state with the correct filters", () => {
      expect(wrapper.state().filters).toEqual(
        expect.objectContaining({
          vehicle_type: "PCO",
          page: 1,
          per_page: 9
        })
      );
    });

    it("initializes an empty results array", () => {
      expect(wrapper.state().results).toEqual([]);
    });

    it("initializes metadata as undefined", () => {
      expect(wrapper.state().metadata).toBe(undefined);
    });
  });

  describe("Mounting the component", () => {
    beforeEach(() => {
      wrapper = shallow(<App />);
    });

    it("calls the service with the correct args", async () => {
      await wrapper.instance().componentDidMount();

      expect(mockRetrieve).toHaveBeenCalledWith({
        page: 1,
        per_page: 9,
        vehicle_type: "PCO"
      });
    });
  });

  describe("buildFilters()", () => {
    beforeEach(() => {
      wrapper.state().filters = {
        vehicle_type: "Consumer",
        page: 2,
        per_page: 9
      };
    });

    it("returns the state filters plus the additional ones specified", () => {
      const filters = wrapper.instance().buildFilters({ some_new_key: 1 });

      expect(filters).toEqual(
        expect.objectContaining({
          vehicle_type: "Consumer",
          page: 2,
          per_page: 9,
          some_new_key: 1
        })
      );
    });

    describe("When additional filters param has a key matching the filters in the original state", () => {
      beforeEach(() => {
        wrapper.state().filters = {
          some_existent_key: "value"
        };
      });

      it("replaces the original state value with the new filter value", () => {
        const filters = wrapper
          .instance()
          .buildFilters({ some_existent_key: "new value" });

        expect(filters).toEqual(
          expect.objectContaining({
            some_existent_key: "new value"
          })
        );
      });
    });
  });

  describe("handlePage()", () => {
    beforeEach(() => {
      wrapper.state().filters = {
        page: 4,
        another_filter: "some value"
      };
    });

    it("calls the retrieve mock with the correct args", async () => {
      await wrapper.instance().handlePage(8);

      expect(mockRetrieve).toHaveBeenCalledWith({
        another_filter: "some value",
        page: 8
      });
    });

    it("updates the filters in the state with the expected values", async () => {
      await wrapper.instance().handlePage(6);

      wrapper.update();

      expect(wrapper.state().filters).toEqual(
        expect.objectContaining({
          another_filter: "some value",
          page: 6
        })
      );
    });
  });

  describe("handleFilter()", () => {
    let aggregation = "vehicle_make";
    let filteredKey = "Skoda";
    let selected = true;

    it("calls the service mock with the expected args", async () => {
      await wrapper.instance().handleFilter(aggregation, filteredKey, selected);

      expect(mockRetrieve).toHaveBeenCalledWith(
        expect.objectContaining({
          page: 1,
          per_page: 9,
          vehicle_make: "Skoda",
          vehicle_type: "PCO"
        })
      );
    });

    it("updates the relevant state values", async () => {
      await wrapper.instance().handleFilter(aggregation, filteredKey, selected);
      wrapper.update();

      expect(wrapper.state().filters).toEqual(
        expect.objectContaining({
          vehicle_type: "PCO",
          page: 1,
          per_page: 9,
          vehicle_make: "Skoda"
        })
      );
    });

    describe("When removing a filter by passing selected = false to the method", () => {
      beforeEach(() => {
        wrapper.state().filters = {
          some_filter: "value"
        };
      });

      it("removes the given filter from the state", async () => {
        await wrapper.instance().handleFilter("some_filter", null, false);

        wrapper.update();

        expect(wrapper.state().filters).toEqual({});
      });
    });
  });
});
