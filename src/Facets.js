import React, { Component } from "react";
import PropTypes from "prop-types";
import humanizeString from "humanize-string";
import _ from "lodash";

import FacetItem from "./FacetItem";

import styles from "./facets.module.css";

export default class Facets extends Component {
  static get nonDisplayFilters() {
    return ["page", "vehicle_type", "per_page"];
  }

  itemSelected = (section, item) => {
    const filteringOnSection = _.includes(
      Object.keys(this.props.filters),
      section
    );

    if (!filteringOnSection) {
      return false;
    }

    if (this.props.filters[section] === item) {
      return true;
    }

    return false;
  };

  vehicleType = () => {
    return (
      <div>
        <select
          className={styles.select}
          value={this.props.filters["vehicle_type"]}
          onChange={e =>
            this.props.handleVehicleTypeChange(e.currentTarget.value)
          }
        >
          <option value="Consumer">Consumer</option>
          <option value="PCO">Private Hire</option>
        </select>
      </div>
    );
  };

  facetChoices = (sectionKey, choices) => {
    return Object.keys(choices).map(key => {
      return (
        <FacetItem
          section={sectionKey}
          value={key}
          handleFilter={this.props.handleFilter}
          selected={this.itemSelected(sectionKey, key)}
          count={choices[key]}
        />
      );
    });
  };

  appliedFilters = () => {
    const applied = Object.keys(this.props.filters).filter(
      item => !_.includes(Facets.nonDisplayFilters, item)
    );
    return applied.map(key => {
      return (
        <span className={styles.appliedFilter}>
          <strong>{humanizeString(key)}:</strong>
          <br />
          <i data-identifier="filteringValue">{this.props.filters[key]}</i>
          <button
            className={styles.removeButton}
            onClick={e =>
              this.props.handleFilter(key, this.props.filters[key], false)
            }
          >
            x
          </button>
        </span>
      );
    });
  };

  aggregations = () => {
    if (!this.props.metadata) {
      return null;
    }
    const { aggregations } = this.props.metadata;

    return Object.keys(aggregations).map((key, index) => {
      const aggregation = aggregations[key];
      const label = humanizeString(key);

      return (
        <fieldset className={styles.facetSection} key={label}>
          <legend className={styles.facetSectionTitle}>{label}</legend>

          <div className={styles.facetOptionsContainer}>
            {this.facetChoices(key, aggregation)}
          </div>
        </fieldset>
      );
    });
  };

  render = () => (
    <section className={styles.facetContainer}>
      <div key="applied" className={styles.appliedFilters}>
        {this.appliedFilters()}
      </div>
      <div key="vehicleType" className={styles.facetItem}>
        {this.vehicleType()}
      </div>
      <div key="aggregations" className={styles.facetItem}>
        {this.aggregations()}
      </div>
    </section>
  );
}

Facets.propTypes = {
  metadata: PropTypes.object.isRequired,
  filters: PropTypes.object.isRequired,
  handleFilter: PropTypes.func.isRequired,
  handleVehicleTypeChange: PropTypes.func.isRequired
};
