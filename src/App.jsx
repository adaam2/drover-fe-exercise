import React, { Component } from "react";
import omit from "lodash/omit";

import Loader from "react-loader";

import Vehicles from "./services/vehicles";
import styles from "./App.module.css";

import Facets from "./Facets";
import VehicleResults from "./VehicleResults";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      results: [],
      metadata: undefined,
      filters: {
        vehicle_type: "PCO",
        page: 1,
        per_page: 9
      },
      loading: false
    };

    this.service = new Vehicles();
    this.handleFilter = this.handleFilter.bind(this);
    this.handlePage = this.handlePage.bind(this);
    this.handleVehicleTypeChange = this.handleVehicleTypeChange.bind(this);
  }

  buildFilters = (additionalKeys: object = {}) => {
    return Object.assign(this.state.filters, additionalKeys);
  };

  updateFilteringState = (results, metadata, filters) => {
    this.setState({
      results,
      metadata,
      filters,
      loading: false
    });
  };

  async componentDidMount() {
    this.setState({ loading: true });

    const { data: results, metadata } = await this.service.retrieve(
      this.state.filters
    );

    this.setState({ results, metadata, loading: false });
  }

  async handleFilter(
    aggregation: string,
    filteredKey: string,
    selected: boolean = true
  ) {
    this.setState({ loading: true });
    const updatedFilters = selected
      ? this.buildFilters({ [aggregation]: filteredKey, page: 1 })
      : omit(this.state.filters, aggregation);
    const { data: results, metadata } = await this.service.retrieve(
      updatedFilters
    );

    this.updateFilteringState(results, metadata, updatedFilters);
  }

  async handleVehicleTypeChange(selectedType: string) {
    this.setState({ loading: true });
    const updatedFilters = this.buildFilters({
      vehicle_type: selectedType,
      page: 1
    });

    const { data: results, metadata } = await this.service.retrieve(
      updatedFilters
    );

    this.updateFilteringState(results, metadata, updatedFilters);
  }

  async handlePage(newPageNumber: number) {
    this.setState({ loading: true });
    const updatedFilters = this.buildFilters({ page: newPageNumber });
    const { data: results, metadata } = await this.service.retrieve(
      updatedFilters
    );

    this.updateFilteringState(results, metadata, updatedFilters);
  }

  render() {
    if (this.state.loading) {
      return <Loader />;
    }

    const { results, metadata } = this.state;
    if (!results || !metadata) {
      return null;
    }

    return (
      <main className={styles.container}>
        <Facets
          handleFilter={this.handleFilter}
          handleVehicleTypeChange={this.handleVehicleTypeChange}
          {...this.props}
          {...this.state}
        />
        <VehicleResults
          currentPage={this.state.filters["page"]}
          handlePage={this.handlePage}
          {...this.state}
        />
      </main>
    );
  }
}

export default App;
