import React from "react";
import PropTypes from "prop-types";

import VehicleCard from "./VehicleCard";
import Pagination from "./Pagination";

import styles from "./vehicle-results.module.css";

const VehicleResults = ({ results, metadata, handlePage, currentPage }) => {
  const resultsList = () => {
    return results.map(result => <VehicleCard key={result.id} {...result} />);
  };

  return (
    <section className={styles.resultsContainer}>
      <div className={styles.flexItem}>
        <Pagination
          currentPage={currentPage}
          handlePage={handlePage}
          metadata={metadata}
        />
      </div>
      <div className={styles.grid}>{resultsList()}</div>
    </section>
  );
};

VehicleResults.propTypes = {
  results: PropTypes.array.isRequired,
  metadata: PropTypes.object.isRequired,
  handlePage: PropTypes.func.isRequired,
  currentPage: PropTypes.number.isRequired
};

export default VehicleResults;
