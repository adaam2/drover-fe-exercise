import React from "react";
import Moment from "moment";

import styles from "./vehicle-card.module.css";

const VehicleCard = ({
  id,
  year,
  images,
  available_start_date: availableFrom,
  topImage = images[0],
  city_name: cityName,
  vehicle_make: make,
  vehicle_model: model
}) => {
  const imagePreview = (
    <div
      className={styles.imageContainer}
      style={{ backgroundImage: `url(${topImage.image_url})` }}
    />
  );

  const availableDate = new Moment(availableFrom);
  const now = new Moment();
  const ctaText = availableDate.isAfter(now)
    ? `Available ${availableDate.fromNow()}`
    : "Available NOW";

  return (
    <div key={id} className={styles.card}>
      {imagePreview}
      <h4 className={styles.cardTitle}>
        {make} - {model} ({year})
      </h4>
      <a href="#some-page" className={styles.cardButton}>
        {ctaText}
      </a>
    </div>
  );
};

export default VehicleCard;
